module.exports = {

    options: {
        spawn: false,
        livereload: true
    },

    js: {
        files: [
            'dev/js/*.js'
        ],
        tasks: [
            'uglify'
        ]
    },

    css: {
        files: [
            'dev/sass/*.scss'
        ],
        tasks: [
            'sass',
            'cssmin'
        ]
    },
};
